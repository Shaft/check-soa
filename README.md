# Check SOA

## Program

`check_soa.py` is a simple program to query the SOA record from all nameservers (or from a list of servers) for a given zone and display the serial number.

Options allow JSON output or a Nagios-compatible output, TCP mode, DNS over TLS mode (with optional certificate verification)... See [usage](#usage) for all options

When giving a list of servers, enable recursion if given servers are recursive servers

## Requirements

- Python ≥ 3.10
- [dnspython](https://www.dnspython.org/) ≥ 2.3.0

## Usage

The possible options are:

### General
- -h, --help: show this very message
- --multi: Use multi-threading for faster results
- --ns `SERVERS`: Name servers to query (comma-separated list). Names or addresses are accepted
- -t `FLOAT`: Timeout for queries (in seconds). Default: 1.5
### Transport
- -4, --ipv4: Use IPv4 only
- -6, --ipv6: Use IPv6 only
- --tcp: Use TCP
- --tls: Use DNS over TLS
### Output
- -m, --monitor: Nagios compatible output
- --json: JSON output
- --time: Add the response time of queries (in ms)
### DNS
- --noedns: Disable EDNS
- --nodnssec: Disable DNSSEC validation for the requests to get the domain's NS
- --nsid: Enable NSID EDNS option
- -r, --recursion: Enable RD bit in queries. Mandatory if recursive servers are given with --ns
### Monitoring
- -n, --no-check: Disable serial check. If servers return different serials, no error is raised
- -w `WARNING`, --warning `WARNING`: Warning threshold (monitor mode. Default: 1)
- -c `CRITICAL`, --critical `CRITICAL`: Critical threshold (monitor mode. Default: 2)
### TLS
- --tls-verify: Verify certificate when in DoT mode
- --ca-cert `/PATH/TO/CERTSTORE` Path to a CA certificate or a directory containing the certificates. Default: `/etc/ssl/certs/`


Some notes:

- Serials are by default compared:
  - In monitoring mode, if multiple serials are spotted `check_soa.py` will consider the highest value as the legitimate one and raise a `WARNING` or `CRITICAL` state.
  - In regular mode, a warning will be added.
  - You can disable this behavior by using the `--no-check` option.
  - JSON output does not compare serials yet.
- In monitor mode, a nameserver is considered "broken" if any of its IP addresses:
    - reaches a timeout when queried
    - returns a DNS error code (`REFUSED`, `SERVFAIL`...)
    - returns an inconsistent serial
- In JSON output, the `duration` in the `additional` section is given in seconds
## LICENSE

GNU GPLv3
