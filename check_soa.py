#!/usr/bin/env python3
# Coypright © John Shaft. See LICENSE for complete GNU GPLv3 text
# Or visit https://framagit.org/Shaft/check-soa/-/blob/main/LICENSE
"""
Simple check_soa program

See README for extended usage

Requires Python >= 3.10
Requires dnspython >= 2.3.0
"""

import sys
import os
import time
import argparse
import ipaddress
import ssl
import json
from typing import Tuple
import concurrent.futures as cf
from pathlib import Path
from dns import resolver, query, message, name, rcode, exception, edns, flags, version, inet

# First things first: testing if both Python and dnspython requirements are met

# Not using f-strings in case of very old Python version

# Testing Python version
prog_name = os.path.basename(__file__)
python_version = sys.version.split()[0]
python_ok = True
if sys.version_info.major < 3:
    python_ok = False
elif sys.version_info.minor < 10:
    # We assume Python 4 is not in the works :)
    python_ok = False

if not python_ok:
    print("ERROR: Python version %s is installed. %s requires Python 3.10.0 or higher" \
        % (python_version, prog_name))
    sys.exit(1)

# Testing dnspython version
if version.hexversion < 33751281: # 2.3.0 hexversion
    print("ERROR: dnspython version %s is installed. %s requires Python 2.3.0 or higher" \
        % (version.version, prog_name))
    sys.exit(1)

def perf_data(tsamp: float) -> str:
    """Makes the perf data string for monitor mode"""
    timer = time.monotonic() - tsamp
    return f"| time={timer:.6f}s;;;;"

def get_ns_addr(tup: Tuple[str, str, resolver.Resolver]) -> Tuple[str, str | Tuple[str, ...] | None]:
    """
    Query the system resolver to get both A and AAAA records for a given name.
    Takes a tuple containing:
        - Name to query (str)
        - IP version (str)
        - a resolver (dns.resolver.Resolver)
    Returns a tuple:
        - The name we queried (str)
        - One of the following:
            - An error message (str)
            - A tuple of str containing 1 or more IP addresses
            - None, if no answer was found (resolver answered NODATA)
    """
    name_server, ip_version, res = tup
    tup_ip = ()
    # If name_server is an address, the job is done, name_server's address is... name_server
    if inet.is_address(name_server):
        return name_server, (name_server,)
    if ip_version == "IPv4":
        rdtype = "A"
    else:
        rdtype = "AAAA"
    try:
        ns_ip = res.resolve(qname=name_server, rdtype=rdtype)
        assert ns_ip.rrset is not None
        for rdata in ns_ip.rrset:
            ip: str = rdata.to_text()
            tup_ip = (*tup_ip, ip)
        return name_server, tup_ip
    except resolver.NoAnswer:
        return name_server, None
    except resolver.NXDOMAIN:
        return name_server, "NXDOMAIN"

def query_ns(tup: Tuple[str, str, str, message.QueryMessage]) -> Tuple[str, str, int | str, float]:
    """
    Query a DNS server for a domain SOA
    Takes a tuple containing:
        - IP address of the DNS server (str)
        - Domain name of the DNS server (str)
        - Transport (str)
        - Query (dns.message.QueryMessage)
    Returns a tuple:
        - Domain name of the DNS server (str)
        - IP address of the DNS server (str)
        - The SOA or an error message (int or str)
        - Query time (float)
    Having hostname and ip in both input and output is redundant but
    makes life easier when working with multithreading
    """
    ip, hostname, transport, msg = tup
    # We need to time execution to provide query time in case of error
    beginning = time.monotonic()
    # If ip not an address then it is an error message
    if not inet.is_address(ip):
        return hostname, ip, "Error", 0
    try:
        if transport == "tls":
            # Defining a context in order to be able to skip cert verification
            context = ssl.SSLContext(protocol=ssl.PROTOCOL_TLS_CLIENT)
            context.load_verify_locations(cafile=cafile, capath=capath)
            if not args.cert_verify:
                context.check_hostname=False
                context.verify_mode=ssl.CERT_NONE
            answer = query.tls(
                q=msg,
                where=ip,
                ssl_context=context, # type: ignore
                server_hostname=hostname,
                timeout=timeout
            )
        elif transport == "tcp":
            answer = query.tcp(
                q=msg,
                where=ip,
                timeout=timeout
            )
        else:
            answer = query.udp(
                q=msg,
                where=ip,
                timeout=timeout
            )
        nsid:str = ""
        query_nsid = False
        try:
            if msg.options[0].otype == 3:
                query_nsid = True
        except IndexError:
            pass
        if query_nsid:
            for opt in answer.options:
                # Checking by option type in case of mutiple options present
                if opt.otype == 3:
                    # dnspython 2.6.0 adds support for the NSID EDNS option which breaks
                    # decoding of the option
                    if version.hexversion >= 33947888: # hexversion is an int, which is good
                        # The to_text() method adds an ungracious "NSID "
                        # But still needs it because we do not know how the NSID
                        # looks like on the wire and the to_text() method checks for us
                        nsid = opt.to_text().replace("NSID ", "", 1)
                    else:
                        nsid = opt.data.decode()
            if nsid != "":
                ip += f" [{nsid}]"
        if answer.rcode() != rcode.NOERROR:
            return hostname, ip, f"Got a {rcode.to_text(answer.rcode())}", answer.time
        # Answer is empty
        if not answer.answer:
            return hostname, ip, "Can't get a SOA. Is server authoritative?", time.monotonic() - beginning
    except ssl.SSLCertVerificationError:
        return hostname, ip, "Certificate verification error", time.monotonic() - beginning
    except exception.Timeout:
        return hostname, ip, "Timeout", timeout
    except ConnectionRefusedError:
        return hostname, ip, "Connection refused", time.monotonic() - beginning
    # Getting SOA from answer
    soa = answer.get_rrset(
        section=answer.answer,
        name=name.from_text(domain),
        rdclass=1,  # type: ignore (IN)
        rdtype=6  # type: ignore (SOA)
    )
    serial:int = soa[0].serial  # type: ignore
    return hostname, ip, serial, answer.time

start = time.monotonic()

parser = argparse.ArgumentParser(
    description="Query all nameservers for a given domain and get SOA's serial"
)
parser.add_argument(
    "--multi",
    help="Use multi-threading for faster results",
    action="store_true"
)
parser.add_argument(
    "--ns",
    help="Name servers to query (comma-separated list). Names or addresses are accepted",
    metavar="SERVERS",
    type=str
)
parser.add_argument(
    "-t",
    dest="timeout",
    help="Timeout for queries (in seconds). Default: 1.5",
    metavar="FLOAT",
    type=float
)
parser.add_argument(
    "-4",
    "--ipv4",
    help="Use IPv4 only",
    action="store_true"
)
parser.add_argument(
    "-6",
    "--ipv6",
    help="Use IPv6 only",
    action="store_true"
)
parser.add_argument(
    "--tcp",
    help="Use TCP",
    action="store_true"
)
parser.add_argument(
    "--tls",
    help="Use DNS over TLS",
    action="store_true"
)
parser.add_argument(
    "-m",
    "--monitor",
    help="Nagios compatible output",
    action="store_true"
)
parser.add_argument(
    "--json",
    help="JSON output",
    action="store_true"
)
parser.add_argument(
    "--time",
    help="Add the response time of queries (in ms)",
    action="store_true"
)
parser.add_argument(
    "--noedns",
    help="Disable EDNS",
    action="store_false"
)
parser.add_argument(
    "--nodnssec",
    help="Disable DNSSEC validation for the requests to get the domain's NS",
    action="store_true"
)
parser.add_argument(
    "--nsid",
    help="Enable NSID EDNS option",
    action="store_true"
)
parser.add_argument(
    "-r",
    "--recursion",
    help="Enable RD bit in queries. Mandatory if recursive servers are given with --ns",
    action="store_true"
)
parser.add_argument(
    "-n",
    "--no-check",
    help="Disable serial check. If servers return different serials, no error \
        is raised",
    action="store_true"
)
parser.add_argument(
    "-w",
    "--warning",
    help="Warning threshold (monitor mode. Default: 1)",
    type=int
)
parser.add_argument(
    "-c",
    "--critical",
    help="Critical threshold (monitor mode. Default: 2)",
    type=int
)
parser.add_argument(
    "--tls-verify",
    dest="cert_verify",
    help="Verify certificate when in DoT mode",
    action="store_true"
)
parser.add_argument(
    "--ca-cert",
    dest="cacert",
    help="Path to a CA certificate or a directory \
        containing the certificates. Default: /etc/ssl/certs/",
    metavar="/PATH/TO/CERTSTORE"
)
parser.add_argument(
    "domain",
    help="Domain to test"
)

args = parser.parse_args()

multi = args.multi

# EDNS
use_edns = args.noedns
no_dnssec = args.nodnssec
options = []
if args.nsid:
    options.append(edns.GenericOption(edns.OptionType.NSID, b""))
if args.ipv4 and args.ipv6:
    print("ERROR: Can't do only IPv4 and only IPv6 at the same time")
    sys.exit(1)

if args.timeout is None:
    timeout = 1.5
else:
    timeout = args.timeout
# Transport
protocol = ""

if args.tcp:
    protocol = "tcp"

# TLS Options
if args.tls:
    protocol = "tls"
    options.append(edns.GenericOption(edns.OptionType.PADDING, b""))
if args.cacert is None:
    capath = "/etc/ssl/certs"
    cafile = None
else:
    if Path(args.cacert).resolve().is_dir():
        capath = args.cacert
        cafile = None
    else:
        capath = None
        cafile = args.cacert

# Output

out_json = args.json

# Monitoring option

monitor = args.monitor

if not args.warning:
    warning = 1
else:
    warning = args.warning

if not args.critical:
    critical = 2
else:
    critical = args.critical

if monitor and out_json:
    print("SOA UNKNOWN - Can't output to JSON and to Nagios")
    sys.exit(3)

# NS options
if args.recursion:
    flag = flags.RD
else:
    flag = 0


domain = args.domain
if domain[0] == "." and len(domain) > 1:
    if monitor:
        print("SOA ERROR - Name starts with a dot. Please remove it")
        sys.exit(3)
    else:
        print("ERROR: Name starts with a dot. Please remove it")
        sys.exit(1)


# Setting up resolver
resol = resolver.Resolver()
if use_edns:
    resol.use_edns(
        edns=True,
        ednsflags=flags.DO,
        payload=1232
    )

if no_dnssec:
    resol.flags = flags.RD + flags.CD

if not args.ns:
    # Looking for NS
    try:
        domain_ns = resol.resolve(
            qname=domain,
            rdtype="NS"
        )
    except resolver.NoAnswer:
        new_dom = resolver.zone_for_name(domain)
        if monitor:
            print(f"SOA UNKNOWN - No NS found, have you tried \"{new_dom}\"?",
            perf_data(start))
            sys.exit(3)
        else:
            print(f"ERROR: No NS found for {domain}, have you tried \"{new_dom}\"?")
            sys.exit(1)
    except resolver.NXDOMAIN:
        if monitor:
            print("SOA UNKNOWN - Name does not exist", perf_data(start))
            sys.exit(3)
        else:
            print(f"ERROR: Name {domain} does not exist")
            sys.exit(1)
    except resolver.NoNameservers:
        if monitor:
            print("SOA CRITICAL - All nameservers failed to answer the query"
            + f" '{domain} IN NS'", perf_data(start))
            sys.exit(2)
        else:
            print(f"ERROR: All nameservers failed to answer the query '{domain} IN NS'")
            sys.exit(1)
    except exception.Timeout:
        if monitor:
            print("SOA CRITICAL - Can't find nameservers (timeout)", perf_data(start))
            sys.exit(2)
        else:
            print(f"ERROR: Can't find nameservers for {domain} (timeout)")
            sys.exit(1)

    list_ns = []

    assert domain_ns.rrset is not None
    for ns in domain_ns.rrset:
        try:
            nsname = ns.target.to_unicode().lower()
        except name.IDNAException:
            nsname = ns.to_text().lower()
        list_ns.append(nsname)
else:
    list_ns = args.ns.lower().split(",")

# Sorting nameservers?
#list_ns.sort()
dict_ip = {}

if __name__ == "__main__":
    family="" # Making linter happy
    items = []
    for n in list_ns:
        # Simplest way to deal with IP given with --ns
        # As get_ns_addr checks if the name given is an address
        # and then does practically nothing (returns a tuple (hostname, (hostname, ))
        if inet.is_address(n):
            if isinstance(ipaddress.ip_address(n), ipaddress.IPv4Address):
                items.append((n, "IPv4", resol))
            else:
                items.append((n, "IPv6", resol))
        else:
            if not args.ipv6:
                family = "IPv4"
                items.append((n, family, resol))
            if not args.ipv4:
                family = "IPv6"
                items.append((n, family, resol))

    if multi:
        with cf.ThreadPoolExecutor(max_workers=len(items)) as thread:
            for ns, result in thread.map(get_ns_addr, items):
                if result is not None:
                    if result == "NXDOMAIN":
                        # dict_ip contains a str and a tuple
                        dict_ip[ns] = (result,)
                    else:
                        try:
                            for res in result:
                                dict_ip[ns] = (*dict_ip[ns], res)
                        except KeyError:
                            dict_ip[ns] = result
                else:
                    pass
    else:
        for ns_query in items:
            ns, result = get_ns_addr(ns_query)
            if result is not None:
                if result == "NXDOMAIN":
                    # dict_ip contains a str and a tuple
                    dict_ip[ns] = (result,)
                else:
                    try:
                        for res in result:
                            dict_ip[ns] = (*dict_ip[ns], res)
                    except KeyError:
                        dict_ip[ns] = result

    if not dict_ip:
        if monitor:
            print(f"SOA CRITICAL - no nameservers accessible with {family}",
            perf_data(start))
            sys.exit(3)
        else:
            print(f"ERROR: no nameservers accessible with {family}")
            sys.exit(1)

    soa_query = message.make_query(
        qname=domain,
        rdtype="SOA",
        use_edns=use_edns,
        options=options,
        payload=1232,
        flags=flag
    )

    nb_ip = 0
    list_broken = set()
    dict_res = {}
    query_list = []

    for ns, ips in dict_ip.items():
        dict_res[ns] = {}
        for addr in ips:
            query_list.append((addr, ns, protocol, soa_query))
            nb_ip += 1

    # There are at least len(items) queries in query_list.
    # Keeping the same pool as earlier (with a max len(items) workers) implies:
    # - If each NS as one IP of each type (one IPv4 & one IPv6) then len(items) == len(query_list)
    # - If one or more NS as multiple IPs (eg. CloudFlare's NS), having len(items) is slower
    if multi:
        with cf.ThreadPoolExecutor(max_workers=len(query_list)) as thread:
            for ns, ip, serial, rtt in thread.map(query_ns, query_list):
                dict_res[ns][ip] = (serial, rtt)
    else:
        for s_query in query_list:
            ns, ip, serial, rtt = query_ns(s_query)
            dict_res[ns][ip] = (serial, rtt)


    # Building the output
    output = ""
    err_out = ""
    nb_err = 0
    list_serial = set()
    nb_servers = len(dict_res.keys())
    exit_status = 0

    list_soa = []
    for ns, dict_data in dict_res.items():
        addresses = []
        for ip_addr, data in dict_data.items():
            ser, qtime = data
            json_addr = {}
            # We split in case of nsid
            json_ip = ip_addr.split()[0]
            json_addr["ip"] = json_ip
            # If ip_addr.split()[1] exists, we have a NSID
            try:
                json_nsid = ip_addr.split()[1].replace("[", "").replace("]","")
            except IndexError:
                json_nsid = None
            iptype = "" # Happy linter is happy
            try:
                if isinstance(ipaddress.ip_address(json_ip), ipaddress.IPv4Address):
                    iptype = "IPv4"
                elif isinstance(ipaddress.ip_address(json_ip), ipaddress.IPv6Address):
                    iptype = "IPv6"
            except ValueError:
                iptype = None
            json_addr["family"] = iptype
            if args.nsid:
                json_addr["nsid"] = json_nsid
            if isinstance(ser, int):
                json_addr["serial"] = ser
            else:
                json_addr["serial"] = None
                if iptype is None:
                    # We most likely have a NXDOMAIN
                    json_addr["ip"] = None
                    json_addr["error"] = json_ip
                else:
                    json_addr["error"] = ser
            if args.time:
                json_addr["query_time"] = round(qtime * 1000, 2)
            addresses.append(json_addr)
        list_soa.append({"nameserver": ns, "addresses": addresses})

    if out_json:
        date_utc = time.strftime("%Y-%m-%d %H:%M:%SZ", time.gmtime())
        duration = round(time.monotonic() - start, 6)
        if protocol == "":
            protocol = "udp"
        json_additional = { "date": date_utc, "duration": duration, "transport": protocol }
        output += json.dumps({"domain": domain,
                              "soa": list_soa,
                              "additional": json_additional },
                             indent=4)
    elif monitor:
        out_serial = None
        incoherent = False

        for entry in list_soa:
            for address in entry["addresses"]:
                if isinstance(address["serial"], int):
                    list_serial.add(address["serial"])
                # If serial is not an int then we must have an error
                else:
                    nb_err += 1
                    list_broken.add(entry["nameserver"])
                    err_out += f"{entry['nameserver']} ({address['ip']}: {address['error']}); "

        # We have now possibly all serials in a set
        if not out_serial and list_serial:
            if args.no_check:
                out_serial = ", ".join(map(str, sorted(list_serial, reverse=True)))
            else:
                out_serial = sorted(list_serial)[-1]
        if len(list_serial) > 1:
            # We have multiple serials!
            incoherent = True
        if incoherent and not args.no_check:
            for entry in list_soa:
                serials = set()
                broken = False
                for address in entry["addresses"]:
                    if address["serial"] != out_serial:
                        broken = True
                if broken:
                    nb_err += 1
                    for address in entry["addresses"]:
                        serials.add(address["serial"])
                    list_broken.add(entry["nameserver"])
                    err_out += f"{entry['nameserver']}: " + ", ".join(map(str, sorted(serials, reverse=True))) +"; "
        if 0 < nb_err < nb_ip:
            if nb_err >= critical:
                global_status = "SOA CRITICAL"
                exit_status = 2
            elif nb_err >= warning:
                global_status = "SOA WARNING"
                exit_status = 1
            else:
                global_status = "SOA OK"
                exit_status = 0
            nb_ok = nb_servers - len(list_broken)
            output += f"{global_status} - "
            if nb_ok > 0:
                output += f"{nb_ok} nameserver(s), serial: {out_serial}, "
            output += f"{len(list_broken)} broken nameserver(s): {err_out[:-2]}"
        else:
            output += f"SOA OK - {nb_servers} nameservers, serial: {out_serial}"
        output += f" {perf_data(start)}"
    else:
        for entry in list_soa:
            output += f"{entry['nameserver']}\n"
            for address in entry["addresses"]:
                if args.nsid and address["nsid"] is not None:
                    str_ip = f"{address['ip']} [{address['nsid']}]"
                else:
                    str_ip = f"{address['ip']}"
                if isinstance(address["serial"], int):
                    status = "OK"
                    list_serial.add(address["serial"])
                    result = str(address["serial"])
                else:
                    status = "KO"
                    nb_err += 1
                    list_broken.add(entry["nameserver"])
                    result = address["error"]
                if args.time:
                    result += f" ({address['query_time']} ms)"
                output += f"\t{str_ip}: {status}: {result}\n"
        if len(list_broken) > 0:
            output += f"\n{len(list_broken)} broken server(s) out of {nb_servers}: "
            output += ", ".join(list_broken)

        if len(list_serial) > 1 and not args.no_check:
            output += "\nWARNING: multiple serials reported by nameservers: "
            output += ", ".join(map(str,sorted(list_serial, reverse=True)))

    if nb_err == nb_ip:
        if monitor:
            print(f"SOA CRITICAL - No answers from nameservers: {err_out[:-2]}",
            perf_data(start))
            sys.exit(2)
        else:
            short_out = "\n".join(output.splitlines()[:-2])
            print(f"ERROR: No answers from nameservers:\n\n{short_out}")
            sys.exit(1)

    print(output.strip("\n"))
    sys.exit(exit_status)
